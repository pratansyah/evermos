package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func firstProblem(writer http.ResponseWriter, request *http.Request) {
	var load firstInput
	body, _ := ioutil.ReadAll(request.Body)
	err := json.Unmarshal(body, &load)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Load the first batch of magazines according to the number of GunMaxMagazines from input
	var gun []int = load.Magazines[0:load.GunMaxMagazines]
	walker := load.GunMaxMagazines

	// Quit as soon as the front most magazine was found to be fully loaded
	for gun[0] != load.MagazineCapacity && walker <= len(load.Magazines)-load.GunMaxMagazines {
		// Load a new magazine
		/*
		 * This is a weird way of solving the problem, but since
		 * a) It was mention specifically in the problem that soldier can put multiple magazine
		 *    into one gun
		 * b) Magazine must be loaded first into gun to find out whether it was full or not
		 *
		 * So the check has to be done in a weird way like this
		 */
		gun = load.Magazines[walker : load.GunMaxMagazines+walker]
		walker++
	}

	// Check the last batch of magazines
	for gun[0] != load.MagazineCapacity && len(gun) > 1 {
		gun = gun[1:]
	}

	writer.Header().Set("Content-Type", "application/json")
	if gun[0] != load.MagazineCapacity {
		json.NewEncoder(writer).Encode(errorStatus{Status: false, Reason: "No full magazine found"})
	} else {
		json.NewEncoder(writer).Encode(firstOutput{Status: true, LoadedGun: gun})
	}
}
