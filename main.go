package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

var storeData store
var keyMap [][]rune
var xBound int
var yBound int

func main() {
	m := mux.NewRouter()
	storeData.products = make(map[int]*product)

	//Listen to the base url and send a response
	m.HandleFunc("/", func(writer http.ResponseWriter, _ *http.Request) {
		writer.WriteHeader(200)
		fmt.Fprintf(writer, "Server is up and running")
	})

	m.HandleFunc("/first", firstProblem).Methods("POST")
	m.HandleFunc("/second", secondProblem).Methods("POST")
	m.HandleFunc("/populateStore", populateStoreWrapper)
	m.HandleFunc("/third", thirdProblem)
	populateStore()
	initiateKeyMap()
	//Start Server
	server := &http.Server{
		Handler: m,
	}
	server.Addr = ":1243"
	server.ListenAndServe()
}
