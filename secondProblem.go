package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func secondProblem(writer http.ResponseWriter, request *http.Request) {
	var load secondInput
	body, _ := ioutil.ReadAll(request.Body)
	err := json.Unmarshal(body, &load)
	if err != nil {
		fmt.Println(err)
		return
	}

	if item, ok := storeData.products[load.ID]; ok {
		writer.Header().Set("Content-Type", "application/json")
		if item.Stock < load.Quantity {
			json.NewEncoder(writer).Encode(errorStatus{Status: false, Reason: "Not enough product in stock"})
		} else {
			// Lock use mutex then proceed the transaction
			storeData.Lock()
			defer storeData.Unlock()
			storeData.products[load.ID].Stock -= load.Quantity
			json.NewEncoder(writer).Encode(secondOutput{Status: true, TotalPrice: load.Quantity * storeData.products[load.ID].Price, StockLeft: storeData.products[load.ID].Stock})
		}
	}
}
