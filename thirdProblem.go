package main

import (
	"encoding/json"
	"net/http"
)

func thirdProblem(writer http.ResponseWriter, request *http.Request) {
	var possibleKey []point

	writer.Header().Set("Content-Type", "application/json")
	if string(keyMap[1][1]) != "#" {
		exploreMap(&possibleKey, "north", 1, 1)
		json.NewEncoder(writer).Encode(possibleKey)
	} else {
		json.NewEncoder(writer).Encode(errorStatus{Status: false, Reason: "Wrong starting point"})
	}
}

func exploreMap(possibleKey *[]point, direction string, y int, x int) {
	if direction == "north" {
		if y+1 < yBound && string(keyMap[y+1][x]) != "#" {
			exploreMap(possibleKey, "north", y+1, x)
		}
		if x+1 < xBound && string(keyMap[y][x+1]) != "#" {
			exploreMap(possibleKey, "east", y, x+1)
		}
	}
	if direction == "east" {
		if x+1 < xBound && string(keyMap[y][x+1]) != "#" {
			exploreMap(possibleKey, "east", y, x+1)
		}
		if y-1 >= 0 && string(keyMap[y-1][x]) != "#" {
			exploreMap(possibleKey, "south", y-1, x)
		}
	}
	if direction == "south" {
		if y-1 >= 0 && string(keyMap[y-1][x]) != "#" {
			exploreMap(possibleKey, "south", y-1, x)
		}
		*possibleKey = append(*possibleKey, point{X: x, Y: y})
	}
}
