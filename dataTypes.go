package main

import "sync"

type firstInput struct {
	MagazineCapacity int
	GunMaxMagazines  int
	Magazines        []int
}

type firstOutput struct {
	Status    bool
	LoadedGun []int
}

type secondInput struct {
	ID       int
	Quantity int
}

type secondOutput struct {
	Status     bool
	TotalPrice int
	StockLeft  int
}

type store struct {
	sync.Mutex
	products map[int]*product
}
type product struct {
	ID    int
	Name  string
	Price int
	Stock int
}

type point struct {
	X int
	Y int
}

type errorStatus struct {
	Status bool
	Reason string
}
