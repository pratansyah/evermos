package main

import (
	"net/http"
	"strings"
)

func populateStore() {
	baygon := product{ID: 1, Name: "Baygon", Price: 5000, Stock: 10}
	lifebuoy := product{ID: 2, Name: "Lifebuoy", Price: 15000, Stock: 8}
	gatsby := product{ID: 3, Name: "Gatsby", Price: 25000, Stock: 7}
	storeData.products[baygon.ID] = &baygon
	storeData.products[lifebuoy.ID] = &lifebuoy
	storeData.products[gatsby.ID] = &gatsby
}

func populateStoreWrapper(writer http.ResponseWriter, request *http.Request) {
	populateStore()
}

func initiateKeyMap() {
	rawMap := `########
	#......#
	#.###..#
	#...#.##
	#X#....#
	########`
	rowOfMaps := strings.Split(rawMap, "\n")
	// Invert the map so we can have a proper x y coordinate
	yBound = len(rowOfMaps)
	for i := len(rowOfMaps) - 1; i >= 0; i-- {
		keyMap = append(keyMap, []rune(strings.TrimSpace(rowOfMaps[i])))
	}
	xBound = len(keyMap[0])
}
